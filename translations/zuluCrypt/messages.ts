<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>CreateVolumeDialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.ui" line="17"/>
        <source>warning!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.ui" line="29"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.ui" line="42"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.ui" line="55"/>
        <source>This operation will lead to permanent destrunction of all present data in /dev/sdc1. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.cpp" line="41"/>
        <source>This operation will lead to permanent destrunction of all present data in &quot;%1&quot;.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolumedialog.cpp" line="69"/>
        <source>It is advised to create encrypted containers over random data to prevent information leakage.

Do you want to write random data to &quot;%1&quot; first before creating an encrypted container in it?

You can stop the random data writing process anytime you want if it takes too long and you can no longer wait.

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CryptTask</name>
    <message>
        <location filename="../../zuluCrypt-gui/crypttask.cpp" line="96"/>
        <source>Calculating md5sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/crypttask.cpp" line="146"/>
        <source>Creating Encrypted Container File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/crypttask.cpp" line="223"/>
        <source>Copying Data To The Container File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/crypttask.cpp" line="283"/>
        <source>Copying Data From The Container File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogMsg</name>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="29"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="42"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="55"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="68"/>
        <source>text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="90"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="109"/>
        <source>cipher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="125"/>
        <source>key size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="141"/>
        <source>device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="160"/>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="179"/>
        <source>offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="195"/>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="243"/>
        <source>size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="211"/>
        <source>mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="227"/>
        <source>fs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="259"/>
        <source>used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="275"/>
        <source>unused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="291"/>
        <source>used %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.ui" line="307"/>
        <source>active slots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="97"/>
        <source>
&quot;system volumes&quot; are volumes that either udev has identify them as such if udev is enabled or have an entry in &quot;/etc/fstab&quot;,&quot;/etc/crypttab&quot; or &quot;/etc/zuluCrypt/system_volumes.list&quot;.

If you prefer for a volume not to be considered a system volume,start the toolfrom root account and then go to &quot;menu-&gt;options-&gt;manage non system partitions&quot; and add the volume to the list and the volume will stop being considered as &quot;system&quot;.

Alternatively,you can add yourself to group &quot;zulucrypt&quot; and &quot;zulumount&quot; and all restrictions will go away.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="106"/>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="122"/>
        <source>INFORMATION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="114"/>
        <source>Insufficient privilege to access a system device,
only root user or members of group zulucrypt can do that</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="116"/>
        <source>Insufficient privilege to access a system device in read/write mode,
only root user or members of group zulucrypt-write can do that</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="119"/>
        <source>You do not seem to have proper permissions to access the encrypted file in %1 mode,check file permissions and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/dialogmsg.cpp" line="295"/>
        <source>Do not show this dialog again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LxQt::Wallet::changePassWordDialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="92"/>
        <source>Create a new wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="94"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="119"/>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="252"/>
        <source>Passwords do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="216"/>
        <source>New passwords do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="197"/>
        <source>Wallet password could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.cpp" line="206"/>
        <source>Wallet could not be opened with the presented key</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LxQt::Wallet::password_dialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.cpp" line="89"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.cpp" line="90"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.cpp" line="99"/>
        <source>wallet could not be opened with the presented key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.cpp" line="156"/>
        <source>Wallet could not be opened with the presented key</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="29"/>
        <source>open encrypted volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="44"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="63"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="82"/>
        <source>Open the volume in &amp;read only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="98"/>
        <source>select mount point path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="117"/>
        <source>open volume path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="194"/>
        <location filename="../../zuluCrypt-gui/password.ui" line="278"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="213"/>
        <source>Mount Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="232"/>
        <source>Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="283"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="288"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="308"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password.ui" line="155"/>
        <source>open key file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../zuluCrypt-gui/utility.cpp" line="140"/>
        <source>
options:
	-d   path to where a volume to be auto unlocked/mounted is located
	-m   tool to use to open a default file manager(default tool is xdg-open)
	-e   start the application without showing the GUI
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/utility.cpp" line="600"/>
        <source>if the option is checked,a primary private mount point will be created in &quot;/run/media/private/$USER/&quot;
and a secondary publicly accessible &quot;mirror&quot; mount point will be created in &quot;/run/media/public/&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/utility.cpp" line="1049"/>
        <source>about zuluCrypt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>changePassWordDialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="14"/>
        <source>Change Wallet&apos;s Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="42"/>
        <source>C&amp;hange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="55"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="126"/>
        <source>Enter Current Password Below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="145"/>
        <source>Enter New Password Below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="164"/>
        <source>Re Enter New Password Below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="183"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="196"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;An application &apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&apos; has made a request for a password of its wallet &apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;%2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&apos; to be changed&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/changepassworddialog.ui" line="214"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Enter password information below to create a new wallet &apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;%1&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&apos; for application &apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;%2&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&apos;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>checkForUpdates</name>
    <message>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="49"/>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="69"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="49"/>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="69"/>
        <source>Failed To Check For Update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="60"/>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="66"/>
        <source>
Installed Version Is : %1.
Latest Version Is : %2.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="61"/>
        <source>Update Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/checkforupdates.cpp" line="67"/>
        <source>Version Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>createfile</name>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="17"/>
        <source>create a container file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="129"/>
        <source>open a folder dialog box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="39"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="68"/>
        <source>File Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="87"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="145"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="164"/>
        <source>% Complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="183"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="206"/>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="211"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.ui" line="216"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="144"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="147"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="150"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="158"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="162"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="165"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="182"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="190"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="197"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="210"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="215"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="158"/>
        <source>Illegal character in the file size field.Only digits are allowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="56"/>
        <source>Create A Container File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="144"/>
        <source>File name field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="147"/>
        <source>File path field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="150"/>
        <source>File size field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="162"/>
        <source>File with the same name and at the destination folder already exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="165"/>
        <source>You dont seem to have writing access to the destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="182"/>
        <source>Container file must be bigger than 3MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="190"/>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="197"/>
        <source>Failed to create volume file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="210"/>
        <source>Operation terminated per user choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="215"/>
        <source>Could not open cryptographic back end to generate random data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="228"/>
        <source>Terminating file creation process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="229"/>
        <source>Are you sure you want to stop file creation process?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createfile.cpp" line="254"/>
        <source>Select Path to where the file will be created</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>createkeyfile</name>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="17"/>
        <source>create a key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="39"/>
        <source>Keyfile Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="58"/>
        <source>path to a folder to create a key in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="71"/>
        <source>open a folder a key file will be created in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="87"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="106"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="125"/>
        <source>Keyfile Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.ui" line="144"/>
        <source>RNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="150"/>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="153"/>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="156"/>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="159"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="150"/>
        <source>The key name field is empth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="153"/>
        <source>Folder path to where the key will be created is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="156"/>
        <source>File with the same name and at the destination folder already exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="159"/>
        <source>You dont seem to have writing access to the destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="211"/>
        <source>Process interrupted,key not fully generated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="214"/>
        <source>Key file successfully created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="221"/>
        <source>Select A Folder To Create A Key File In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="211"/>
        <source>WARNING!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createkeyfile.cpp" line="214"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>createvolume</name>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="20"/>
        <source>create a new volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="118"/>
        <source>open a key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="42"/>
        <source>Path to Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="61"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="80"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="134"/>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="287"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="83"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="86"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="163"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="165"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="502"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="547"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="163"/>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="306"/>
        <source>Repeat Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="246"/>
        <source>Volume Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="348"/>
        <source>Volume Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="378"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="383"/>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="388"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="426"/>
        <source>Volume Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="445"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="464"/>
        <source>RNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="461"/>
        <source>random number generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.ui" line="483"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="76"/>
        <source>Options are separated by a &quot;.&quot; character.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="77"/>
        <source>Multiple algorithms are separated by &quot;:&quot; character.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="78"/>
        <source>Options are in a format of &quot;algorithm.cipher mode.key size in bits.hash&quot;

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="79"/>
        <source>Default option is the first entry on the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="84"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="87"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="164"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="166"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="514"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="557"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="98"/>
        <source>Normal TrueCrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="99"/>
        <source>Normal+Hidden TrueCrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="101"/>
        <source>Normal VeraCrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="102"/>
        <source>Normal+Hidden VeraCrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="139"/>
        <source>Passphrase Quality: 0%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="141"/>
        <source>Passphrase Quality: %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="144"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="491"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="536"/>
        <source>Create A New Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="219"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="220"/>
        <source>TrueCrypt Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="228"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="229"/>
        <source>VeraCrypt Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="246"/>
        <source>Path To Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="251"/>
        <source>Path To File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="399"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="407"/>
        <source>Keyfile Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="526"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="568"/>
        <source>Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="650"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="658"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="665"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="671"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="675"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="692"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="703"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="832"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="833"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="835"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="836"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="837"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="838"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="839"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="840"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="841"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="842"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="843"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="844"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="845"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="846"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="847"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="848"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="849"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="850"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="851"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="852"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="853"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="854"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="855"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="856"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="857"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="650"/>
        <source>Volume path field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="658"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="675"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="703"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="665"/>
        <source>Illegal character detected in the hidden volume size field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="671"/>
        <source>Hidden passphrases do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="692"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="845"/>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="853"/>
        <source>Passphrases do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="810"/>
        <source>Please be patient as creating a VeraCrypt volume may take a very long time.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="825"/>
        <source>Volume created successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="827"/>
        <source>
Creating a backup of the &quot;%1&quot; volume header is strongly advised.
Please read documentation on why this is important.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="832"/>
        <source>Presented file system is not supported,see documentation for more information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="835"/>
        <source>Could not create an encrypted volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="836"/>
        <source>Could not open volume for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="837"/>
        <source>There seem to be an opened mapper associated with the device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="838"/>
        <source>Can not create a volume on a mounted device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="839"/>
        <source>Container file must be bigger than 3MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="841"/>
        <source>Insufficient memory to hold your response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="842"/>
        <source>Operation terminated per user request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="843"/>
        <source>Could not get passphrase in silent mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="844"/>
        <source>Insufficient memory to hold the passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="846"/>
        <source>Invalid path to key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="847"/>
        <source>Could not get a key from a key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="848"/>
        <source>Couldnt get enought memory to hold the key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="849"/>
        <source>Could not get a key from a socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="850"/>
        <source>One or more required argument(s) for this operation is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="851"/>
        <source>Can not get passphrase in silent mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="852"/>
        <source>Insufficient memory to hold passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="854"/>
        <source>Failed to create a volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="855"/>
        <source>Wrong argument detected for tcrypt volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="856"/>
        <source>Could not find any partition with the presented UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="857"/>
        <source>Unrecognized ERROR! with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="206"/>
        <source>TrueCrypt keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="681"/>
        <source>It is best to create a hidden volume with vfat/fat file system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="681"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="212"/>
        <source>VeraCrypt keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="830"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="833"/>
        <source>insufficient privilege to open a system device in read/write mode,
only root user or members of group zulucrypt can do that</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/createvolume.cpp" line="840"/>
        <source>%1 not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cryptfiles</name>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="235"/>
        <source>Path to source field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="240"/>
        <source>Invalid path to source file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="243"/>
        <source>Destination path already taken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="252"/>
        <source>First key field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="256"/>
        <source>Second key field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="259"/>
        <source>Keys do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="265"/>
        <source>Invalid path to key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="417"/>
        <source>Enter A Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="433"/>
        <source>Enter A Path To A Keyfile Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="434"/>
        <source>keyfile path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="450"/>
        <source>Select A File You Want To Encrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="453"/>
        <source>Select A File You Want To Decrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="463"/>
        <source>Select A Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="482"/>
        <source>Encrypted file created successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="485"/>
        <source>Decrypted file created successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="487"/>
        <source>Could not open keyfile for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="489"/>
        <source>Could not open encryption routines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="491"/>
        <source>File or folder already exist at destination address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="494"/>
        <source>Insufficient privilege to create destination file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="496"/>
        <source>Presented key did not match the encryption key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="498"/>
        <source>Operation terminated per user request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="500"/>
        <source>Insufficient privilege to open source file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="502"/>
        <source>Decrypted file created successfully but md5 checksum failed,file maybe corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="504"/>
        <source>Could not open reading encryption routines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="506"/>
        <source>Could not open writing encryption routines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="508"/>
        <source>Failed to close encryption routine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="235"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="240"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="243"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="252"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="256"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="259"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="265"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="487"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="489"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="491"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="494"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="496"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="500"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="504"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="506"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="508"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="142"/>
        <source>Create An Encrypted Version Of A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="151"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="163"/>
        <source>Create A Decrypted Version Of An encrypted File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="391"/>
        <source>Select Path to put destination file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="452"/>
        <source>zuluCrypt encrypted files ( *.zc ) ;; All Files ( * )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="482"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="485"/>
        <source>SUCCESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="498"/>
        <source>INFO!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="502"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="39"/>
        <source>Destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="74"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="93"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="148"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="191"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="424"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="153"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="220"/>
        <location filename="../../zuluCrypt-gui/cryptfiles.cpp" line="425"/>
        <source>Repeat Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="239"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptfiles.ui" line="265"/>
        <source>% Complete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>cryptoinfo</name>
    <message>
        <location filename="../../zuluCrypt-gui/cryptoinfo.ui" line="17"/>
        <source>Greetings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptoinfo.ui" line="29"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptoinfo.ui" line="42"/>
        <source>Please consult &quot;menu-&gt;help-&gt;open zuluCrypt.pdf&quot; to get an introduction on zuluCrypt.

Unity users,the menu is on the upper left corner of the screen when zuluCrypt has focus.

Project&apos;s homepage is at:
https://mhogomchungu.github.io/zuluCrypt

Recommending reading FAQ page from the project&apos;s main page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/cryptoinfo.ui" line="71"/>
        <source>Do not show this message again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>erasedevice</name>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.ui" line="17"/>
        <source>erase data on the device by writing random data over them</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.ui" line="29"/>
        <source>Path to Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.ui" line="74"/>
        <source>% Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.ui" line="106"/>
        <source>&amp;Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.ui" line="122"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="77"/>
        <source>The next dialog will write random data to a device leading to permanent loss of all contents on the device.

Are you sure you want to continue? </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="45"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="98"/>
        <source>Write Random Data Over Existing Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="81"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="104"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="104"/>
        <source>Data on the device successfully erased</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="105"/>
        <source>Could not create mapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="106"/>
        <source>Could not resolve device path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="107"/>
        <source>Random data successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="109"/>
        <source>Operation terminated per user choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="110"/>
        <source>Can not write on a device with opened mapper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="111"/>
        <source>Policy prevents non root user opening mapper on system partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="113"/>
        <source>Device path is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="114"/>
        <source>Passphrase file does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="115"/>
        <source>Could not get enought memory to hold the key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="116"/>
        <source>Insufficient privilege to open key file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="117"/>
        <source>This device appear to already be in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="118"/>
        <source>Can not open a mapper on a mounted device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="119"/>
        <source>Could not write to the device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="138"/>
        <source>Device path field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="144"/>
        <source>Invalid path to device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="155"/>
        <source>Writing Random Data Over Existing Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="242"/>
        <source>Enter Path To Volume To Be Erased</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="251"/>
        <source>Select A Non System Partition To Erase Its Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="105"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="106"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="107"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="109"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="110"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="111"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="113"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="114"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="115"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="116"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="117"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="118"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="119"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="138"/>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="144"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="147"/>
        <source>Are you really sure you want to write random data to &quot;%1&quot; effectively destroying all contents in it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/erasedevice.cpp" line="150"/>
        <source>WARNING!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>favorites</name>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="17"/>
        <source>manage favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="29"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="64"/>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="89"/>
        <source>Volume ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="72"/>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="108"/>
        <source>Mount Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="147"/>
        <source>open partition dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="163"/>
        <source>open file dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.ui" line="179"/>
        <source>&amp;Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="157"/>
        <source>Remove Selected Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="160"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="204"/>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="207"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="204"/>
        <source>Device address field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="207"/>
        <source>Mount point path field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/favorites.cpp" line="224"/>
        <source>Path To An Encrypted Volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>luksaddkey</name>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="20"/>
        <source>add a key to a volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="35"/>
        <source>Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="54"/>
        <source>open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="70"/>
        <source>open partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="101"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="127"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="239"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="284"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="198"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="220"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="114"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="252"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="176"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="195"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="323"/>
        <source>Reenter Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="343"/>
        <source>Key Already In The Encrypted Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="359"/>
        <source> Key To Be Added To The Encrypted Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="146"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.ui" line="265"/>
        <source>open keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="102"/>
        <source>Passphrase Quality: 0%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="104"/>
        <source>Passphrase Quality: %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="107"/>
        <source>Add A Key To A Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="158"/>
        <source>Existing Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="167"/>
        <source>New Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="176"/>
        <source>Encrypted Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="206"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="230"/>
        <source>Enter a path to a keyfile location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="209"/>
        <source>Keyfile Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="217"/>
        <source>Enter a key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="233"/>
        <source>Keyfile path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="252"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="259"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="265"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="269"/>
        <source>Keys do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="334"/>
        <source>Key added successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="336"/>
        <source>Key added successfully.
%1 / %2 slots are now in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="352"/>
        <source>Presented key does not match any key in the volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="353"/>
        <source>Could not open luks volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="354"/>
        <source>Volume is not a luks volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="355"/>
        <source>Insufficient privilege to add a key to a system device,
only root user or members of group &quot;zulucrypt&quot; can do that
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="356"/>
        <source>Could not open volume in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="357"/>
        <source>All key slots are occupied, can not add any more keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="358"/>
        <source>Can not get passphrase in silent mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="359"/>
        <source>Insufficient memory to hold passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="360"/>
        <source>New passphrases do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="361"/>
        <source>One or more required argument(s) for this operation is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="362"/>
        <source>One or both keyfile(s) does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="363"/>
        <source>Insufficient privilege to open key file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="364"/>
        <source>Couldnt get enought memory to hold the key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="365"/>
        <source>Could not get a key from a socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="366"/>
        <source>Could not get elevated privilege,check binary permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="367"/>
        <source>Can not find a partition that match presented UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="368"/>
        <source>Device is not a luks device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="369"/>
        <source>Unrecognized ERROR! with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="252"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="259"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="265"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="269"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="352"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="353"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="354"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="355"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="356"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="357"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="358"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="359"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="360"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="361"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="362"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="363"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="364"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="365"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="366"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="367"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="368"/>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="369"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="195"/>
        <source>Enter A Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksaddkey.cpp" line="341"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>luksdeletekey</name>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="20"/>
        <source>remove a key from a volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="103"/>
        <source>open a keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="45"/>
        <source>Key </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="76"/>
        <source>Existing Key In The Volume To Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="89"/>
        <source>Existing Key From A Keyfile To Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="122"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="141"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="170"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="284"/>
        <source>Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="189"/>
        <source>open an encrypted file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.ui" line="208"/>
        <source>open an encrypted partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="191"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="206"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="256"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="257"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="258"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="260"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="261"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="262"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="263"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="264"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="265"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="266"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="267"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="268"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="269"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="270"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="271"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="85"/>
        <source>Enter a key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="86"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="119"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="96"/>
        <source>Enter a path to a keyfile location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="97"/>
        <source>Keyfile path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="106"/>
        <source>Key File With A Passphrase To Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="191"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="206"/>
        <source>Volume is not a luks volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="209"/>
        <source>There is only one last key in the volume.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="210"/>
        <source>
Deleting it will make the volume unopenable and lost forever.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="211"/>
        <source>
Are you sure you want to delete this key?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="213"/>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="218"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="217"/>
        <source>Are you sure you want to delete a key from this volume?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="253"/>
        <source>Key removed successfully.
%1 / %2 slots are now in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="256"/>
        <source>There is no key in the volume that match the presented key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="257"/>
        <source>Could not open the volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="258"/>
        <source>Insufficient privilege to open a system device,only root user or members of group zulucrypt can do that</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="260"/>
        <source>Could not open the volume in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="261"/>
        <source>Insufficient memory to hold your response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="262"/>
        <source>Operation terminated per user request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="263"/>
        <source>Can not get passphrase in silent mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="264"/>
        <source>Insufficient memory to hold passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="265"/>
        <source>One or more required argument(s) for this operation is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="266"/>
        <source>Keyfile does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="267"/>
        <source>Could not get enough memory to open the key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="268"/>
        <source>Insufficient privilege to open key file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="269"/>
        <source>Could not get a key from a socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="270"/>
        <source>Can not find a partition that match presented UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="271"/>
        <source>Unrecognized ERROR! with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/luksdeletekey.cpp" line="254"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>manageSystemVolumes</name>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.ui" line="17"/>
        <source>manage system volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.ui" line="32"/>
        <source>&amp;Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.ui" line="51"/>
        <source>Add Fi&amp;le</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.ui" line="70"/>
        <source>Add Dev&amp;ice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.ui" line="108"/>
        <source>Path To System Volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="124"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="124"/>
        <source>Could not open &quot;%1&quot; for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="156"/>
        <source>Remove Selected Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="159"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="174"/>
        <source>Are you sure you want to remove 
&quot;%1&quot;
 from the list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="207"/>
        <source>Select Path To System Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managesystemvolumes.cpp" line="178"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>managevolumeheader</name>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="17"/>
        <source>backup volume header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="39"/>
        <source>Backup Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="74"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="93"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="112"/>
        <source>Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="205"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="234"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="221"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="266"/>
        <source>Normal Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="282"/>
        <source>Windows System Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="295"/>
        <source>Whole Drive Encrypted Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="321"/>
        <source>Manage A LUKS Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.ui" line="337"/>
        <source>Manage A TrueCrypt Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="335"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="338"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="468"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="469"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="470"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="472"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="473"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="474"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="475"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="476"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="477"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="478"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="479"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="480"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="481"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="482"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="483"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="484"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="487"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="489"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="492"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="160"/>
        <source>Enter an existing key in the back up header file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="161"/>
        <source>Restore volume header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="163"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="170"/>
        <source>Enter an existing key in the volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="171"/>
        <source>Back up volume header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="173"/>
        <source>&amp;Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="335"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="338"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="373"/>
        <source>Are you sure you want to replace a header on device &quot;%1&quot; with a backup copy at &quot;%2&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="375"/>
        <source>WARNING!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="432"/>
        <source>Select luks container you want to backup its header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="456"/>
        <source>Header restored successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="468"/>
        <source>Oresented device is not a LUKS device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="469"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="470"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="473"/>
        <source>Failed to perform requested operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="471"/>
        <source>Operation terminater per user request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="472"/>
        <source>Path to be used to create a back up file is occupied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="474"/>
        <source>Insufficient privilege to open backup header file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="475"/>
        <source>Invalid path to back up header file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="476"/>
        <source>Insufficient privilege to create a backup header in a destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="477"/>
        <source>Invalid path to device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="478"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="479"/>
        <source>Argument for path to a backup  header file is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="480"/>
        <source>Only root user and &quot;zulucrypt&quot; members can restore and back up luks headers on system devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="481"/>
        <source>Insufficient privilege to open device for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="482"/>
        <source>Could not resolve path to device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="483"/>
        <source>Backup file does not appear to contain luks header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="484"/>
        <source>Insufficient privilege to open device for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="487"/>
        <source>Wrong password entered or volume is not a truecrypt volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="489"/>
        <source>Failed to perform requested operation on the LUKS volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="492"/>
        <source>Unrecognized ERROR! with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="454"/>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="456"/>
        <source>SUCCESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="227"/>
        <source>Select A File With A LUKS Backup Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="232"/>
        <source>Select A Folder To Store The Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="454"/>
        <source>Header saved successfully.
If possible,store it securely.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/managevolumeheader.cpp" line="471"/>
        <source>INFO!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>oneinstance</name>
    <message>
        <location filename="../../zuluMount-gui/oneinstance.cpp" line="115"/>
        <source>There seem to be another instance running,exiting this one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluMount-gui/oneinstance.cpp" line="108"/>
        <source>Previous instance seem to have crashed,trying to clean up before starting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>openvolume</name>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="17"/>
        <source>select a partition to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="35"/>
        <source>Use UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="48"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="64"/>
        <source>Use &amp;UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="80"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="142"/>
        <source>partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="150"/>
        <source>size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="158"/>
        <source>label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="166"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.ui" line="174"/>
        <source>uuid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="120"/>
        <source>A list of all partitions on this system are displayed here.
Double click an entry to use it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="123"/>
        <source>Restart the tool from root&apos;s account or after you have created and added yourself to group &quot;zulucrypt&quot; if the volume you want to use is not on the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="125"/>
        <source>You are a root user and all partitions are displayed.
Double click an entry to use it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="128"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="163"/>
        <source>Select A Partition To Create An Encrypted Volume In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="169"/>
        <source>Select An Encrypted Partition To Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="269"/>
        <source>Only crypto_LUKS volumes can be selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/openvolume.cpp" line="269"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>passwordDialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="521"/>
        <source>&quot;/&quot; character is not allowed in mount name field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="129"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="139"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="195"/>
        <source>VeraCrypt Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="198"/>
        <source>TrueCrypt Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="233"/>
        <source>Unlock VeraCrypt Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="235"/>
        <source>Unlock Encrypted Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="297"/>
        <source>Choose A Module From The File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="298"/>
        <source>Enter A Module Name To Use To Get Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="301"/>
        <source>Plugin Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="305"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="347"/>
        <source>Select A Key Module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="313"/>
        <source>Enter A Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="317"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="327"/>
        <source>Choose A Key File From The File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="328"/>
        <source>Enter A path To A Keyfile Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="332"/>
        <source>Keyfile Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="345"/>
        <source>Select A Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="358"/>
        <source>Select Path To Mount Point Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="376"/>
        <source>Select Encrypted volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="413"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="432"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="515"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="521"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="543"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="556"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="661"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="678"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="679"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="680"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="681"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="682"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="683"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="684"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="685"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="686"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="687"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="688"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="689"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="690"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="691"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="692"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="693"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="694"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="695"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="696"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="697"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="698"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="699"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="700"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="702"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="413"/>
        <source>Internal wallet is not configured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="432"/>
        <source>The volume does not appear to have an entry in the wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="515"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="543"/>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="556"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="661"/>
        <source>An error has occured and the volume could not be opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="678"/>
        <source>Failed to mount ntfs/exfat file system using ntfs-3g,is ntfs-3g/exfat package installed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="679"/>
        <source>There seem to be an open volume accociated with given address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="680"/>
        <source>No file or device exist on given path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="681"/>
        <source>Volume could not be opened with the presented key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="682"/>
        <source>Insufficient privilege to mount the device with given options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="683"/>
        <source>Insufficient privilege to open device in read write mode or device does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="684"/>
        <source>Only root user can perform this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="686"/>
        <source>Could not create mount point, invalid path or path already taken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="687"/>
        <source>Shared mount point path aleady taken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="688"/>
        <source>There seem to be an opened mapper associated with the device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="689"/>
        <source>Could not get a passphrase from the module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="690"/>
        <source>Could not get passphrase in silent mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="691"/>
        <source>Insufficient memory to hold passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="692"/>
        <source>One or more required argument(s) for this operation is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="693"/>
        <source>Invalid path to key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="694"/>
        <source>Could not get enought memory to hold the key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="695"/>
        <source>Insufficient privilege to open key file for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="696"/>
        <source>Could not get a passphrase through a local socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="697"/>
        <source>Failed to mount a filesystem:invalid/unsupported mount option or unsupported file system encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="698"/>
        <source>Could not create a lock on /etc/mtab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="699"/>
        <source>Insufficient privilege to open a system volume.

Consult menu-&gt;help-&gt;permission for more informaion
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="700"/>
        <source>A non supported device encountered,device is missing or permission denied
Possible reasons for getting the error are:
1.Device path is invalid.
2.The device has LVM or MDRAID signature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="702"/>
        <source>Unrecognized ERROR with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/password_dialog.cpp" line="685"/>
        <source>-O and -m options can not be used together</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>password_dialog</name>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="14"/>
        <source>lxqt wallet service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="42"/>
        <source>The application &apos;%1&apos; has requested to open the wallet &apos;%2&apos;.Enter the password below for this wallet to unlock it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="74"/>
        <source>Wallet &apos;%1&apos; does not exist,do you want to create it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="90"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Wallet &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;&apos;%1&apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt; does not exist, do you want to create it?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="107"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;An application &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt;&apos;%1&apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt; has requested to open a wallet&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:600;&quot;&gt; &apos;%2&apos;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;. Enter the password below for this wallet to unlock it.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="125"/>
        <source>&amp;Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="141"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="167"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/lxqt_wallet/frontend/password_dialog.ui" line="154"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>readOnlyWarning</name>
    <message>
        <location filename="../../zuluCrypt-gui/readonlywarning.ui" line="14"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/readonlywarning.ui" line="26"/>
        <source>Do Not Show This Message Again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/readonlywarning.ui" line="39"/>
        <source>Setting This Option Will Cause The Volume To Open In Read Only Mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/readonlywarning.ui" line="58"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>tcrypt</name>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="17"/>
        <source>TrueCrypt keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="29"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="42"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="68"/>
        <source>Add &amp;Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="112"/>
        <source>Keyfile Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="148"/>
        <source>Enter A Passphrase Below To Be Used To Open The Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="84"/>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="115"/>
        <source>drag and drop key files to add them to the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.ui" line="132"/>
        <source>Enter key files below to be used to open the volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.cpp" line="79"/>
        <source>set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.cpp" line="121"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.cpp" line="121"/>
        <source>At least one keyfile is required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/tcrypt.cpp" line="141"/>
        <source>Select A Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>utility::veraCryptWarning</name>
    <message>
        <location filename="../../zuluCrypt-gui/utility.h" line="416"/>
        <source>Elapsed time: 0 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/utility.h" line="424"/>
        <source>Elapsed time: %0 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/utility.h" line="426"/>
        <source>Elapsed time: %0 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/utility.h" line="437"/>
        <source>Please be patient as unlocking a VeraCrypt volume may take a very long time.

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>walletconfig</name>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="17"/>
        <source>Manage Volumes In A Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="32"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="51"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="64"/>
        <source>Do&amp;ne</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="111"/>
        <source>Volume ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.ui" line="116"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.cpp" line="87"/>
        <source>Are you sure you want to delete a volume with an id of &quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.cpp" line="87"/>
        <source>WARNING!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.cpp" line="165"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfig.cpp" line="165"/>
        <source>Failed To Add the Key In The Wallet.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>walletconfiginput</name>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="14"/>
        <source>add an entry to wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="26"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="39"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="64"/>
        <source>Volume ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="83"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="102"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.ui" line="121"/>
        <source>Repeat Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.cpp" line="90"/>
        <source>Atleast one required field is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.cpp" line="90"/>
        <location filename="../../zuluCrypt-gui/walletconfiginput.cpp" line="96"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.cpp" line="96"/>
        <source>Passphrases do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/walletconfiginput.cpp" line="116"/>
        <source>Select A Volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>zuluCrypt</name>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="20"/>
        <source>zuluCrypt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="84"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="158"/>
        <source>&amp;zC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="185"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="193"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="204"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="218"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="229"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="383"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="237"/>
        <source>Ctrl+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="245"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="250"/>
        <source>crypto info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="253"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="473"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="261"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="269"/>
        <source>Ctrl+K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="74"/>
        <source>Encrypted Volume Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="79"/>
        <source>Encrypted Volume Mount Point Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="102"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="112"/>
        <source>&amp;Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="120"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="129"/>
        <source>&amp;Volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="139"/>
        <source>O&amp;ptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="153"/>
        <source>&amp;Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="182"/>
        <source>PLAIN,LUKS,TrueCrypt Container In A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="201"/>
        <source>Encrypted Container In A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="226"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="234"/>
        <source>Add A Key To A Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="242"/>
        <source>Delete A Key From A Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="258"/>
        <source>Keyfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="266"/>
        <source>Tray Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="274"/>
        <source>Select Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="277"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="282"/>
        <source>favorite volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="287"/>
        <source>manage favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="292"/>
        <source>select random number generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="295"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="367"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="300"/>
        <source>close application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="303"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="407"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="308"/>
        <source>Update Volume List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="311"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="316"/>
        <source>Minimize To Tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="324"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="288"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="332"/>
        <source>Close All Opened Volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="348"/>
        <source>Erase Data In A Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="356"/>
        <source>Backup Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="364"/>
        <source>Restore Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="436"/>
        <source>Manage Volumes In KDE Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="444"/>
        <source>Manage Volumes In GNOME keyring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="452"/>
        <source>Change Internal Wallet Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="470"/>
        <source>VeraCrypt Container In A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="478"/>
        <source>VeraCrypt Container In A Hard Drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="486"/>
        <source>Open ZuluCrypt.pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="489"/>
        <source>Shift+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="494"/>
        <source>Check For Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="497"/>
        <source>Shift+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="319"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="190"/>
        <source>PLAIN,LUKS,TrueCrypt Container In A Hard Drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="215"/>
        <source>Encrypted Container In A Hard Drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="327"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="335"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="340"/>
        <source>Manage Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="343"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="351"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="359"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="375"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="481"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="391"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="399"/>
        <source>Ctrl+J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="420"/>
        <source>configure wallets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="439"/>
        <source>Shift+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="447"/>
        <source>Alt+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="455"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="460"/>
        <source>tcrypt backup header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="465"/>
        <source>tcrypt restore header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="415"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="423"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="372"/>
        <source>permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="380"/>
        <source>Encrypt A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="388"/>
        <source>Decrypt A File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="396"/>
        <source>Header Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="404"/>
        <source>Manage System Volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="412"/>
        <source>Manage Volumes In Internal Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="428"/>
        <source>Manage Non System Volumes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.ui" line="431"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="361"/>
        <source>Restore Volume Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="362"/>
        <source>Backup Volume Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="407"/>
        <source>Failed to open zuluCrypt.pdf,make sure your system can open pdf files using &quot;%1&quot; tool and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="407"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="854"/>
        <source>WARNING!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="678"/>
        <source>Resetting font size to %1 because larger font sizes do not fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="678"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="743"/>
        <source>
LUKS,TrueCrypt and VeraCrypt based encrypted volumes have what is called a &quot;volume header&quot;.

A volume header is responsible for storing information necessary to open a header using encrypted volume and any damage to it will makes it impossible to open the volume causing permanent loss of encrypted data.

The damage to the header is usually caused by accidental formatting of the device or use of some buggy partitioning tools or wrongly reassembled logical volumes.

Having a backup of the volume header is strongly advised because it is the only way the encrypted data will be accessible again after the header is restored if the header on the volume get corrupted.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="752"/>
        <source>Important Information On Volume Header Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="790"/>
        <source>Volume is not open or was opened by a different user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="792"/>
        <source>Volume Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="821"/>
        <source>List Is Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="854"/>
        <source>Could not open mount point because &quot;%1&quot; tool does not appear to be working correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="868"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="872"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="876"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="882"/>
        <source>Add Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="883"/>
        <source>Remove Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="885"/>
        <source>Backup LUKS Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="901"/>
        <source>Add To Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="916"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="958"/>
        <source>Close failed, volume is not open or was opened by a different user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="959"/>
        <source>Close failed, one or more files in the volume are in use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="960"/>
        <source>Close failed, volume does not have an entry in /etc/mtab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="961"/>
        <source>Close failed, could not get a lock on /etc/mtab~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="962"/>
        <source>Close failed, volume is unmounted but could not close mapper,advice to close it manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="963"/>
        <source>Close failed, could not resolve full path of device
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="964"/>
        <source>Close failed, shared mount point appear to be busy
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="965"/>
        <source>Close failed, shared mount point appear to belong to a different user
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="966"/>
        <source>Close failed, shared mount point appear to be in an ambiguous state,advice to unmount manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="967"/>
        <source>Close failed, could not find any partition with the presented UUID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="968"/>
        <source>Unrecognized error with status number %1 encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="790"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="958"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="959"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="960"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="961"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="962"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="963"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="964"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="965"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="966"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="967"/>
        <location filename="../../zuluCrypt-gui/zulucrypt.cpp" line="968"/>
        <source>ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
